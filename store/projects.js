const state = () => ({
  projects: [],
  url: process.env.API_URL || 'http://localhost:1337',
});

const getters = {
  getUrl: (state) => state.url,
  getAll: (state) => state.projects,
  getProjectsForHome: (state) => state.projects.filter((p) => p.home),
  getProjectsByCategory: (state) => (category) =>
    state.projects.filter(
      (p) =>
        p.categories &&
        p.categories.filter((cat) => cat.slug === category).length
    ),
  hasProjects: (state) => state.projects.length,
  getOne: (state) => (uuid) => state.projects.filter((p) => p.uuid === uuid)[0],
};

const mutations = {
  setProjects: (state, projects) => (state.projects = projects),
};

const actions = {
  async fetchProjects({ commit, getters }) {
    const projects = await this.$axios.$get(`${getters.getUrl}/projects`);
    const actualites = await this.$axios.$get(`${getters.getUrl}/actualites`);
    const promotions = await this.$axios.$get(`${getters.getUrl}/promotions`);

    commit(
      'setProjects',
      [...projects, ...actualites, ...promotions].sort(
        (a, b) => new Date(b.published_at) - new Date(a.published_at)
      )
    );
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions,
};

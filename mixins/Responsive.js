export default {
  mounted() {
    window.addEventListener('resize', this.screenTest);
    this.screenTest();
  },

  destroyed() {
    window.removeEventListener('resize', this.screenTest);
  },

  data() {
    return {
      isDesktop: false,
    };
  },

  methods: {
    screenTest() {
      this.isDesktop = window && window.innerWidth >= 1024;
    },
  },
};

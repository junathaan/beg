import createPersistedState from 'vuex-persistedstate';

export default ({ store }) => {
  createPersistedState({
    storage: sessionStorage,
    key: 'beg',
    paths: ['projects'],
  })(store);
};
